export interface SearchParamsUser {
  UUID?: string,
  publicKey?: string,
  nameContains?: string,
  emailContains?: string,
  fromDate?: Date,
}

export default SearchParamsUser;
