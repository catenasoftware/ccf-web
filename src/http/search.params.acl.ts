export interface SearchParamsAcl {
  publicKey?: string,
  showAll?: boolean
}
