import https, {RequestOptions} from "https";
import {Response} from "../apimodels/response";

export default class CustomHttpClient{

  async call(options: RequestOptions, expectJsonResponse: boolean, body?: object): Promise<Response<any>>{
    return new Promise<any>((resolve) => {
      const req = https.request(options, (res) => {
        res.setEncoding('utf8');
        let responseBody = '';


        res.on('data', (chunk) => {
          responseBody += chunk;
        });

        res.on('end', () => {
          if(res.statusCode < 200 || res.statusCode > 300){
            resolve(new Response(false, null, res.statusCode));
            return;
          }
          if(expectJsonResponse) {
            const res = Object.setPrototypeOf(JSON.parse(responseBody), Response.prototype);
            res.isOk = true;
            resolve(res);
          }else{
            resolve(new Response());
          }
        });
      });

      req.on('error', (err) => {
        resolve(new Response(false, err));
      });

      if(body){
        req.write(JSON.stringify(body));
      }
      req.end();
    });
  }
}
