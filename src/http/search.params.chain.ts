export interface SearchParamsChain {
  ownerPublicKey?: string,
  nameContains?: string,
  descriptionContains?: string,
  publicWrite?: boolean,
  publicRead?: boolean,
  closed?: boolean,
}
