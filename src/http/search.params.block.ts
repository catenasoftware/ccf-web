export interface SearchParamsBlock {
  senderPublicKey?: string,
  recipientPublicKey?: string,
  typeID?: number,
}
