export default class EncryptionData {

  encryptedData: string;
  iv: string;
  symmetricKey: string;

  /**
   * @param encryptedData Base64 format
   * @param iv Base64 format
   * @param symmetricKey Base64 format
   */
  constructor(encryptedData: string, iv: string, symmetricKey: string){
    this.encryptedData = encryptedData;
    this.iv = iv;
    this.symmetricKey = symmetricKey;
  }
}
