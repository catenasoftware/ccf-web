import AsymmetricModule, {KeyAlgorithm} from "./asymmetric.module";
import Signature from "../apimodels/signature";
import CCFRsaKeyPair, {CatenaKeyFormat, CatenaKeyType, CCFRsaKey} from "./rsa_key";

const { subtle } = require('crypto').webcrypto ? require('crypto').webcrypto : window.crypto.subtle;

export default class RsaModule extends AsymmetricModule{


  private  publicCryptoKeySign: CryptoKey;
  private  privateCryptoKeySign?: CryptoKey;

  private  publicCryptoKeyEncryption: CryptoKey;
  private  privateCryptoKeySignEncryption?: CryptoKey;

  private static keyType: string = 'RSA-PSS';

  constructor() {
    super();
  }

   static getSubtle(){
    if(subtle == null){
      return window.crypto.subtle;
    }
    return subtle;
  }

  async importKeys(ccfRsaKeyPair: CCFRsaKeyPair): Promise<RsaModule>{

    this.publicCryptoKeySign = await RsaModule.getSubtle().importKey('spki', Buffer.from(ccfRsaKeyPair.publicKey.key, 'base64'), {name: RsaModule.keyType, hash: 'SHA-256' }, true, ['verify'])
    if(ccfRsaKeyPair.privateKey != null){
      this.privateCryptoKeySign = await RsaModule.getSubtle().importKey('pkcs8', Buffer.from(ccfRsaKeyPair.privateKey.key, 'base64'), {name: RsaModule.keyType, hash: 'SHA-256' }, true, ['sign'])
    }

    // Encryption keys
    this.publicCryptoKeyEncryption = await RsaModule.getSubtle().importKey('spki', Buffer.from(ccfRsaKeyPair.publicKey.key, 'base64'), {name: 'RSA-OAEP', hash: 'SHA-256' }, true, ['encrypt'])
    if(ccfRsaKeyPair.privateKey != null){
      this.privateCryptoKeySignEncryption = await RsaModule.getSubtle().importKey('pkcs8', Buffer.from(ccfRsaKeyPair.privateKey.key, 'base64'), {name: 'RSA-OAEP', hash: 'SHA-256' }, true, ['decrypt'])
    }
    if(this.privateCryptoKeySign != null){
      this.cryptId = await this.signBuffer(await this.getPublicKeyBuffer());
    }
    return this;
  }


  static async generateKeyPair(modulusLength: number = 2048, hash: string = 'SHA-256'){
    return await RsaModule.getSubtle().generateKey({
      name: this.keyType,
      modulusLength: modulusLength,
      publicExponent: new Uint8Array([1, 0, 1]),
      hash: hash,
    }, true, ['sign', 'verify']);
  }

  static async generateKeyPairDer(modulusLength: number = 2048, hash: string = 'SHA-256'): Promise<CCFRsaKeyPair>{
    const {publicKey, privateKey} = await RsaModule.generateKeyPair(modulusLength, hash);
    const ePublicKey = await RsaModule.getSubtle().exportKey('spki', publicKey);
    const ePrivateKey = await RsaModule.getSubtle().exportKey('pkcs8', privateKey);
    return new CCFRsaKeyPair(
      new CCFRsaKey(Buffer.from(ePublicKey).toString('base64'), CatenaKeyType.spki, CatenaKeyFormat.der),
      new CCFRsaKey(Buffer.from(ePrivateKey).toString('base64'), CatenaKeyType.pkcs8, CatenaKeyFormat.der)
    );
  }

  async exportKeys(): Promise<CCFRsaKeyPair> {
    const ePublicKey = await RsaModule.getSubtle().exportKey('spki', this.publicCryptoKeySign);
    const ePrivateKey = await RsaModule.getSubtle().exportKey('pkcs8', this.privateCryptoKeySign);
    return new CCFRsaKeyPair(
      new CCFRsaKey(Buffer.from(ePublicKey).toString('base64'), CatenaKeyType.spki, CatenaKeyFormat.der),
      new CCFRsaKey(Buffer.from(ePrivateKey).toString('base64'), CatenaKeyType.pkcs8, CatenaKeyFormat.der)
    );
  }

  getAlgorithm(): KeyAlgorithm {
    return KeyAlgorithm.RSA_WITH_AES_128_CBC;
  }

  async getPublicKeyBase64(): Promise<string> {
    return Buffer.from(await RsaModule.getSubtle().exportKey('spki', this.publicCryptoKeySign)).toString('base64');
  }


  isPublicKeyModule(): Boolean {
    return this.isPublic;
  }

  /**
   * @param data Base64 encoded string
   * @returns Encrypted data base64 encoded
   */
  async encrypt(data: string): Promise<string>{
    let encrypted =  await RsaModule.getSubtle().encrypt(
      {
        name: "RSA-OAEP"
      },
      this.publicCryptoKeyEncryption,
      Buffer.from(data, 'base64')
    );
    return Buffer.from(encrypted).toString('base64')
  }

  /**
   * @param data Base64 encoded string
   * @returns Encrypted data base64 encoded
   */
  async decrypt(data: string): Promise<string>{
    let decrypted =  await RsaModule.getSubtle().decrypt(
      {
        name: "RSA-OAEP"
      },
      this.privateCryptoKeySignEncryption,
      Buffer.from(data, 'base64')
    );
    return Buffer.from(decrypted).toString('base64')
  }

    async sign(message: string): Promise<Signature> {
    const signature =  await RsaModule.getSubtle().sign({ "name": RsaModule.keyType, "saltLength": 0 }, this.privateCryptoKeySign, Buffer.from(message))
    return new Signature(
      Buffer.from(signature).toString('base64'),
      this.getAlgorithm(),
      await this.getPublicKeyBase64()
    );
  }


  async verify(verifiableData: string, signature: string): Promise<boolean> {
    return await RsaModule.getSubtle().verify(
      { "name": RsaModule.keyType, "saltLength": 0 },
          this.publicCryptoKeySign,
          Buffer.from(signature, 'base64'),
          Buffer.from(verifiableData))
  }

  async  signBuffer(buffer: Buffer): Promise<Signature> {
    const signature =  await RsaModule.getSubtle().sign({ "name": RsaModule.keyType, "saltLength": 0 }, this.privateCryptoKeySign, buffer)
    return new Signature(
      Buffer.from(signature).toString('base64'),
      this.getAlgorithm(),
      await this.getPublicKeyBase64()
    );
  }

  async getPublicKeyBuffer(): Promise<Buffer> {
    return Buffer.from(await this.getPublicKeyBase64(), "base64");
  }

  getPublicCryptoKey(){
    return this.publicCryptoKeySign;
  }

  getPrivateCryptoKey(){
    return this.privateCryptoKeySign;
  }

}
