
/** The sweet and fleshy product of a tree or other plant. */
export default class CCFRsaKeyPair{
  readonly privateKey?: CCFRsaKey;
  readonly publicKey: CCFRsaKey;

  constructor(publicKey: CCFRsaKey, privateKey?: CCFRsaKey){
    this.privateKey = privateKey;
    this.publicKey = publicKey;
  }
}

/**
 * @enum
 */
export enum CatenaKeyFormat{
  der = 'der',
}

/**
 * @enum
 */
export enum CatenaKeyType{
  spki = 'spki',
  pkcs1 = 'pkcs1',
  pkcs8 = 'pkcs8'
}

export class CCFRsaKey{
  readonly key: string;
  readonly keyType: CatenaKeyType;
  readonly keyFormat: CatenaKeyFormat;

  constructor(key: string, keyType: CatenaKeyType, keyFormat: CatenaKeyFormat){
    this.key = key;
    this.keyType = keyType;
    this.keyFormat = keyFormat;
  }

}
