import AsymmetricModule, {KeyAlgorithm} from "./asymmetric.module";
import RsaModule from "./rsa.module";
import EccPrime256v1Module from "./ecc.prime256v1.module";
import CCFRsaKeyPair, {CatenaKeyFormat, CatenaKeyType, CCFRsaKey} from "./rsa_key";

export abstract class AsymmetricModuleFactory{

   static async createAsymmetricModule(algorithm: KeyAlgorithm, publicKey: string): Promise<AsymmetricModule>{
    if (algorithm === KeyAlgorithm.ECDH_WITH_AES_128_CBC){
      return new EccPrime256v1Module(publicKey);
    } else if(algorithm == KeyAlgorithm.RSA_WITH_AES_128_CBC){
      const keyPair = new CCFRsaKeyPair(
        new CCFRsaKey(publicKey, CatenaKeyType.spki, CatenaKeyFormat.der),
      );
      return await new RsaModule().importKeys(keyPair);
    }
    throw new Error("Unsupported Algorithm");
  }

  static async createRsaModule(publicKey: string): Promise<RsaModule>{
      const keyPair = new CCFRsaKeyPair(
        new CCFRsaKey(publicKey, CatenaKeyType.spki, CatenaKeyFormat.der),
      );
      return await new RsaModule().importKeys(keyPair);
  }
}
