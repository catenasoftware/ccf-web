// import CryptoJS from "crypto-js";

// import crypto from "crypto"



import EncryptionData from "./encryption.data";

const { subtle } = require('crypto').webcrypto ? require('crypto').webcrypto : window.crypto.subtle;
const crypto = require('crypto').webcrypto;

export default class AESService{

  private readonly algorithm = 'AES-CBC';

  constructor() {

  }

  static getSubtle(){
    if(subtle == null){
      return window.crypto.subtle;
    }
    return subtle;
  }

  static getCrypto(){
    if(crypto == null){
      return window.crypto;
    }
    return crypto;
  }

  async generateKey(): Promise<CryptoKey>{
    return AESService.getSubtle().generateKey(
      {
        name: this.algorithm,
        length: 256
      },
      true,
      ["encrypt", "decrypt"]
    );
  }

  /**
   * @param data Base64 string which should be encrypted
   * @returns EncryptionData
   */
   async encrypt(data: string): Promise<EncryptionData> {
    let key = await this.generateKey();
    let iv = AESService.getCrypto().getRandomValues(new Uint8Array(16));
     let encryptedData =  await AESService.getSubtle().encrypt(
      {
        name: this.algorithm,
        iv
      },
      key,
      Buffer.from(data, 'base64')
    );
     let ivBase64String = Buffer.from(iv).toString('base64');
     let encryptedDatabase64String = Buffer.from(encryptedData).toString('base64');
     let exportedKey = await AESService.getSubtle().exportKey('raw', key);
     let keyBase64String = Buffer.from(exportedKey).toString('base64');
     return new EncryptionData(encryptedDatabase64String, ivBase64String, keyBase64String);
  }

  /**
   * @param rawKey Symmetric key in base64 format
   * @returns CryptoKey
   */
  async importKey(rawKey: string): Promise<CryptoKey>{
    return AESService.getSubtle().importKey(
      "raw",
      Buffer.from(rawKey, 'base64'),
      this.algorithm,
      true,
      ["encrypt", "decrypt"]
    );
  }

  /**
   * @param encryptionData
   * @returns Decrypted base64 string
   */
  async decrypt(encryptionData: EncryptionData): Promise<string>{
    let cryptoKey = await this.importKey(encryptionData.symmetricKey);
    let decrypted = await AESService.getSubtle().decrypt(
      {
        name: this.algorithm,
        iv: Buffer.from(encryptionData.iv, 'base64')
      },
      cryptoKey,
      Buffer.from(encryptionData.encryptedData, 'base64')
    );
    return Buffer.from(decrypted).toString('base64');
  }
}
