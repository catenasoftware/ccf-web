import Signature from "../apimodels/signature";
import CCFRsaKeyPair from "./rsa_key";

export enum KeyAlgorithm{
  RSA_WITH_AES_128_CBC = "RSA_WITH_AES_128_CBC",
  ECDH_WITH_AES_128_CBC  = "ECDH_WITH_AES_128_CBC",
  PLAIN  = "PLAIN"
}
export default abstract class AsymmetricModule {


  protected isPublic = false;

  protected cryptId: Signature;

  abstract sign(message: string): Promise<Signature>;

  abstract verify(message: string, signature: string): Promise<boolean>;

  abstract isPublicKeyModule(): Boolean;

  abstract exportKeys(): Promise<CCFRsaKeyPair>;

  abstract signBuffer(buffer: Buffer): Promise<Signature>;

  abstract getPublicKeyBuffer(): Promise<Buffer>;

  abstract getPublicKeyBase64(): Promise<string>;

  getCryptId(): Signature {
    return this.cryptId;
    // console.log('get crypt id');
    // return await this.signBuffer(await this.getPublicKeyBuffer());
    // console.log('get crypt id');
    // console.log(this.cryptId);
    // if(this.cryptId != null){
    //   return this.cryptId;
    // }
    // this.cryptId = await this.signBuffer(await this.getPublicKeyBuffer());
    // console.log('get crypt id generated');
    // console.log(this.cryptId);
    // return this.cryptId;
  }

  abstract getAlgorithm(): KeyAlgorithm;

  abstract decrypt(data: string): Promise<string>;
  abstract encrypt(data: string): Promise<string>;

  abstract getPublicCryptoKey(): CryptoKey;
  abstract getPrivateCryptoKey(): CryptoKey;
}

