import AsymmetricModule, {KeyAlgorithm} from "./asymmetric.module"
import Signature from "../apimodels/signature";
import CCFRsaKeyPair from "./rsa_key";

const ECKey = require('ec-key');

export default class EccPrime256v1Module extends AsymmetricModule{
  decrypt(data: string): Promise<string> {
      throw new Error("Method not implemented.");
  }
  encrypt(data: string): Promise<string> {
      throw new Error("Method not implemented.");
  }
  getPublicCryptoKey(): CryptoKey {
      throw new Error("Method not implemented.");
  }
  getPrivateCryptoKey(): CryptoKey {
      throw new Error("Method not implemented.");
  }

  private privateKey
  private publicKey

  constructor(publicKey?: string, privateKey?: string) {
    super();
    // If public Key and private Key are provided
    if(publicKey && privateKey){
      this.publicKey = new ECKey(publicKey, 'rfc5280');
      this.privateKey = new ECKey(privateKey, 'pkcs8');
    }
    // If only public key is provided
    else if(publicKey && !privateKey){
      this.publicKey = new ECKey(publicKey, 'rfc5280');
      this.isPublic = true;
    }
    // If no keys are provided, create new some
    else{
      this.privateKey = ECKey.createECKey();
      this.publicKey = this.privateKey.asPublicECKey();
    }
  }

  async sign(message: string, hashAlgorithm: string = 'SHA256'): Promise<Signature> {
    return new Signature(
      this.privateKey.createSign(hashAlgorithm)
      .update(message)
      .sign('base64'),
      this.getAlgorithm(),
      await this.getPublicKeyBase64());
  }

  async signBuffer(message: Buffer, hashAlgorithm: string = 'SHA256'): Promise<Signature> {
    return new Signature(
      this.privateKey.createSign(hashAlgorithm)
        .update(message)
        .sign('base64'),
      this.getAlgorithm(),
      await this.getPublicKeyBase64());
  }

  async verify(message: string, signature: string, hashAlgorithm: string = 'SHA256'): Promise<boolean> {
    return this.publicKey.createVerify(hashAlgorithm)
      .update(message)
      .verify(signature, 'base64');
  }

  isPublicKeyModule(): boolean{
    return this.isPublic;
  }

  exportKeys(): Promise<CCFRsaKeyPair> {
    // return {
      // publicKey: this.publicKey.toString('rfc5280'),
      // privateKey: this.isPublicKeyModule() ? '' : this.privateKey.toString('pkcs8')
    // };
    return null;
  }

  async  getPublicKeyBuffer(): Promise<Buffer>{
    return this.publicKey.toBuffer('rfc5280');
  }

  getAlgorithm(): KeyAlgorithm {
    return KeyAlgorithm.ECDH_WITH_AES_128_CBC;
  }

  async getPublicKeyBase64(): Promise<string> {
    return this.publicKey.toString('rfc5280');
  }
}
