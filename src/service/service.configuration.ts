import AsymmetricModule from "../crypto/asymmetric.module";
import Session from "../apimodels/session";
import Signature from "../apimodels/signature";

export default class ServiceConfiguration{

  public host: string;
  public port: number;
  public signingModuleClient: AsymmetricModule;
  public rejectUnauthorized: boolean;
  public session: Session;
  public cryptIdCCM: Signature;
  public subscriptionID: string;

  constructor(host: string,
              port: number,
              signingModuleClient: AsymmetricModule,
              rejectUnauthorized: boolean = true,
              subscriptionID: string,
              session?: Session,
              cryptIdCCM?: Signature) {
    this.host = host;
    this.port = port;
    this.signingModuleClient = signingModuleClient;
    this.rejectUnauthorized = rejectUnauthorized;
    this.subscriptionID = subscriptionID;
    this.session = session;
    this.cryptIdCCM = cryptIdCCM;
  }
}
