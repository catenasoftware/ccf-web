import ServiceConfiguration from "./service.configuration";
import CustomHttpClient from "../http/http.client";
import {RequestOptions} from "https";

export enum Method{
  GET = "GET",
  POST = "POST",
  PUT = "PUT",
  DELETE = "DELETE"
}

export default abstract class AbstractService{


  protected serviceConfiguration: ServiceConfiguration;
  protected httpClient: CustomHttpClient;

  protected constructor(serviceConfiguration: ServiceConfiguration) {
    this.serviceConfiguration = serviceConfiguration;
    this.httpClient = new CustomHttpClient();
  }

  protected createRequestOptions(path: string, method: Method, token?: string): RequestOptions{
    return {
      hostname: this.serviceConfiguration.host,
      port: this.serviceConfiguration.port,
      path: path,
      method: method,
      rejectUnauthorized: this.serviceConfiguration.rejectUnauthorized,
      headers: {"X-API-SessionToken": token? token : "", 'Content-Type': 'application/json; charset=utf-8'}
    };
  }
}
