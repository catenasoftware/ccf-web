import {RequestOptions} from "https";
import ServiceConfiguration from "./service.configuration";
import AbstractService, {Method} from "./abstract.service";
import {Response} from "../apimodels/response";
import Signature from "../apimodels/signature";
import Session from "../apimodels/session";

export default class SessionService extends AbstractService{

  constructor(serviceConfiguration: ServiceConfiguration) {
    super(serviceConfiguration);
  }

  async createNewConnectToken(): Promise<Response<string>> {
    const requestOptions: RequestOptions = this.createRequestOptions('/session/connecttoken', Method.GET);
    return await this.httpClient.call(requestOptions, true);
  }

  async createNewSessionResponse(): Promise<Response<Session>> {

    const connectTokenServer = await this.createNewConnectToken();
    const token = connectTokenServer.data

    const body = await this.serviceConfiguration.signingModuleClient.sign(token);


    const requestOptions: RequestOptions = this.createRequestOptions('/session/' + token, Method.POST);
    let res = await this.httpClient.call(requestOptions, true, body);
    if(res.isOk){
      res.data = Object.setPrototypeOf(res.data, Session.prototype);
    }
    return res;
  }

  async createNewSession(): Promise<Response<Session>>{
      return await this.createNewSessionResponse();
  }

  async getCryptIdCCMFromCCM(): Promise<Response<Signature[]>>{
      const requestOptions: RequestOptions = this.createRequestOptions('/cryptids', Method.GET);
      return  await this.httpClient.call(requestOptions, true);
  }
}
