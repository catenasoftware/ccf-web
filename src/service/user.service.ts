import AbstractService, {Method} from "./abstract.service";
import ServiceConfiguration from "./service.configuration";
import {Response} from "../apimodels/response";
import UserBase from "../apimodels/user.base";
import {RequestOptions} from "https";
import User from "../apimodels/user";
import BlockBase from "../apimodels/blockBase";
import {KeyAlgorithm} from "../crypto/asymmetric.module";
import {SearchParamsUser} from "../http/search.params.user";
import CreationRequest from "../apimodels/creation.request";
const { v4: uuidv4 } = require('uuid');
const qs = require('qs');

export default class UserService extends AbstractService {

  constructor(serviceConfiguration: ServiceConfiguration) {
    super(serviceConfiguration);
  }

  async createUser(): Promise<Response<User[]>>{
    const creationRequest = new CreationRequest();

    creationRequest.subscriptionID = this.serviceConfiguration.subscriptionID;
    creationRequest.signature = this.serviceConfiguration.signingModuleClient.getCryptId();

    const requestOptions: RequestOptions = this.createRequestOptions('/user', Method.POST);
    return this.httpClient.call(requestOptions, true, creationRequest);
  }

  async getUserByPublicKey(publicKey: string): Promise<Response<User[]>>{
    const params = qs.stringify({publicKey: publicKey});
    const requestOptions: RequestOptions = this.createRequestOptions('/user?' + params, Method.GET, this.serviceConfiguration.session.token);
    return this.httpClient.call(requestOptions, true);
  }

  async getUserById(uuid: string): Promise<Response<User[]>>{
    const params = qs.stringify({UUID: uuid});
    const requestOptions: RequestOptions = this.createRequestOptions('/user?' + params, Method.GET, this.serviceConfiguration.session.token);
    return this.httpClient.call(requestOptions, true);
  }

  async updateUser(name: string, email?: string, applicationData?: string): Promise<Response<void>>{

    const userResponse = await this.getUserByPublicKey(this.serviceConfiguration.signingModuleClient.getCryptId().publicKey)
    if(userResponse.data.length  == 0){
      return new Response<void>(false);
    }

    const user = userResponse.data[0];

    let userBase = new UserBase();
    userBase.name = name;
    userBase.email = email;
    userBase.id = user.id;
    userBase.additionalData = applicationData;
    userBase.publicKey = this.serviceConfiguration.signingModuleClient.getCryptId().publicKey;

    const body = new BlockBase(
      user.chainID,
      uuidv4(),
      this.serviceConfiguration.signingModuleClient.getCryptId(),
      this.serviceConfiguration.cryptIdCCM,
      5,
      KeyAlgorithm.PLAIN,
      btoa(JSON.stringify(userBase)),
    );

    body.signedHashSender = await this.serviceConfiguration.signingModuleClient.sign(body.getJcsPlain());

    const requestOptions: RequestOptions = this.createRequestOptions('/user/' + user.id, Method.PUT);
    return this.httpClient.call(requestOptions, false, body);
  }

  async deleteUser(uuid: string): Promise<Response<void>>{
    const requestOptions: RequestOptions = this.createRequestOptions('/user/' + uuid, Method.DELETE, this.serviceConfiguration.session.token);
    return this.httpClient.call(requestOptions, false);
  }

  async getUsers(searchParamsUser?: SearchParamsUser): Promise<Response<User[]>>{
    const params = qs.stringify(searchParamsUser);
    const requestOptions: RequestOptions = this.createRequestOptions('/users' + (params != null ? '?' + params : ''), Method.GET, this.serviceConfiguration.session.token);
    return this.httpClient.call(requestOptions, true);
  }

}
