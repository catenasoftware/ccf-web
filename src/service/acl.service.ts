import AbstractService, {Method} from "./abstract.service";
import ServiceConfiguration from "./service.configuration";
import BlockBase from "../apimodels/blockBase";
import {KeyAlgorithm} from "../crypto/asymmetric.module";
import {AclRequest} from "../apimodels/acl.request";
import {RequestOptions} from "https";
import {AclEntryRequest} from "../apimodels/acl.entry.request";
import {SearchParamsAcl} from "../http/search.params.acl";
import {Response} from "../apimodels/response";
import ACL from "../apimodels/acl";
const { v4: uuidv4 } = require('uuid');
const qs = require('qs');

export default class AclService extends AbstractService{

  constructor(serviceConfiguration: ServiceConfiguration) {
    super(serviceConfiguration);
  }


  async getAcl(chainId: string, searchParamsAcl?: SearchParamsAcl): Promise<Response<ACL>> {
    const params = searchParamsAcl ? qs.stringify(searchParamsAcl) : '';
    const requestOptions: RequestOptions = this.createRequestOptions(
      '/chains/' + chainId + '/acl?' + params,
      Method.GET,
      this.serviceConfiguration.session.token);

    return this.httpClient.call(requestOptions, true);
  }

  async putAclKeepOther(chainId: string, aclEntryRequests: AclEntryRequest[]): Promise<Response<void>> {
    return this.putAcl(chainId, 3, aclEntryRequests);
  }

  async putAclDeleteOther(chainId: string, aclEntryRequests: AclEntryRequest[]): Promise<Response<void>> {
    return this.putAcl(chainId, 2, aclEntryRequests);
  }


  async putAcl(chainId: string, type: number, aclEntryRequests: AclEntryRequest[]): Promise<Response<void>> {

    const payload = new AclRequest();
    payload.chainId = chainId;
    payload.aclEntryRequests = aclEntryRequests;

    const body = new BlockBase(
      chainId,
      uuidv4(),
      await this.serviceConfiguration.signingModuleClient.getCryptId(),
      this.serviceConfiguration.cryptIdCCM,
      type, KeyAlgorithm.PLAIN, btoa(JSON.stringify(payload)));

      body.signedHashSender = await this.serviceConfiguration.signingModuleClient.sign(body.getJcsPlain());

    const requestOptions: RequestOptions = this.createRequestOptions('/chains/' + chainId + '/acl',
      Method.PUT);

    return this.httpClient.call(requestOptions, false, body);
  }
}
