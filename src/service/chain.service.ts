import AbstractService, {Method} from "./abstract.service";
import ServiceConfiguration from "./service.configuration";
import {RequestOptions} from "https";
import {Response} from "../apimodels/response";
import ChainInfoBase from "../apimodels/chain.info.base";
import ChainInfo from "../apimodels/chain.info";
import BlockBase from "../apimodels/blockBase";
const { v4: uuidv4 } = require('uuid');
import {KeyAlgorithm} from "../crypto/asymmetric.module";
import {SearchParamsChain} from "../http/search.params.chain";
import CreationRequest from "../apimodels/creation.request";
import ChainInfoRequest from "../apimodels/chain.info.request";
const qs = require('qs');

export class ChainService extends AbstractService{

  constructor(serviceConfiguration: ServiceConfiguration) {
    super(serviceConfiguration);
  }

  async createChain(): Promise<Response<ChainInfo[]>>{
    const creationRequest = new CreationRequest();

    creationRequest.signature = await this.serviceConfiguration.signingModuleClient.getCryptId();
    creationRequest.subscriptionID = this.serviceConfiguration.subscriptionID;

    const requestOptions: RequestOptions = this.createRequestOptions('/chains', Method.POST);
    return this.httpClient.call(requestOptions, true, creationRequest)
  }

  async getChainById(id: string): Promise<Response<ChainInfo[]>>{
    const requestOptions: RequestOptions = this.createRequestOptions('/chains/' + id, Method.GET, this.serviceConfiguration.session.token);
    return this.httpClient.call(requestOptions, true);
  }

  async updateChain(chainInfoBase: ChainInfoBase): Promise<Response<void>>{

    // Create a ChainInfoRequest from the give ChainInfoBase
    const chainUpdateRequest = new ChainInfoRequest();
    chainUpdateRequest.id = chainInfoBase.id;
    chainUpdateRequest.name =chainInfoBase.name;
    chainUpdateRequest.description = chainInfoBase.description;
    chainUpdateRequest.closed = chainInfoBase.closed;
    chainUpdateRequest.publicRead = chainInfoBase.publicRead;
    chainUpdateRequest.publicWrite = chainInfoBase.publicWrite;
    chainUpdateRequest.cryptIDOwner = await this.serviceConfiguration.signingModuleClient.getCryptId();
    chainUpdateRequest.subscriptionID = this.serviceConfiguration.subscriptionID;

    let transactionBase = new BlockBase(
      chainInfoBase.id,  // chainId
      uuidv4(), // transactionId
      await this.serviceConfiguration.signingModuleClient.getCryptId(), // cryptIDSender
      this.serviceConfiguration.cryptIdCCM, //cryptIDRecipient
      1, //type
      KeyAlgorithm.PLAIN, // algorithm
      btoa(JSON.stringify(chainUpdateRequest)) //payload base64 encoded
    );

    transactionBase.signedHashSender = await this.serviceConfiguration.signingModuleClient.sign(transactionBase.getJcsPlain());
    const requestOptions: RequestOptions = this.createRequestOptions('/chains/' + chainInfoBase.id, Method.PUT);

    return this.httpClient.call(requestOptions, false, transactionBase);
  }

  async getChains(searchParamsChain: SearchParamsChain): Promise<Response<ChainInfo[]>>{
    const params = qs.stringify(searchParamsChain);
    const requestOptions: RequestOptions = this.createRequestOptions('/chains?' + params, Method.GET, this.serviceConfiguration.session.token);
    return this.httpClient.call(requestOptions, true);
  }
}
