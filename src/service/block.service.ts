import AbstractService, {Method} from "./abstract.service";
import ServiceConfiguration from "./service.configuration";
import {RequestOptions} from "https";
import {Response} from "../apimodels/response";
import BlockBase from "../apimodels/blockBase";
import Signature from "../apimodels/signature";
import {KeyAlgorithm} from "../crypto/asymmetric.module";
import Block from "../apimodels/block";
import {SearchParamsBlock} from "../http/search.params.block";
import AESService from "../crypto/aes";
import {AsymmetricModuleFactory} from "../crypto/asymmetric.module.factory";
const { v4: uuidv4 } = require('uuid');
const qs = require('qs');

export default class BlockService extends AbstractService {

  constructor(serviceConfiguration: ServiceConfiguration) {
    super(serviceConfiguration);
  }

  private buildBlockObjects(blocks: Block[] ): Block[]{
    let res: Block[] = [];
    if(blocks && blocks.length > 0){
      blocks.forEach((block: Block) => {
        res.push(Object.setPrototypeOf(block, Block.prototype))
      })
    }
    return res;
  }

  async createBlock(chainID: string, cryptIDSender: Signature, cryptIDRecipient: Signature, type: number, payload: string, encryptPayload: boolean = false): Promise<Response<string>>{

    const blockID = uuidv4();
    const requestOptions: RequestOptions = this.createRequestOptions('/chains/' + chainID + '/blocks', Method.POST);

    let blockPayload;
    let iv;
    let symmetricKey;
    let symmetricKeySender;
    let symmetricKeyReceiver;
    let keyAlgorithm;

    if(encryptPayload){
      const aes = new AESService();
      const encryptedPayload = await aes.encrypt(btoa(payload));
      blockPayload = encryptedPayload.encryptedData;
      iv = encryptedPayload.iv;
      symmetricKey = encryptedPayload.symmetricKey;
      keyAlgorithm = KeyAlgorithm.RSA_WITH_AES_128_CBC;
    }else{
      blockPayload = btoa(payload);
      keyAlgorithm = KeyAlgorithm.PLAIN;
    }

    if(symmetricKey != null){
      symmetricKeySender =  await this.serviceConfiguration.signingModuleClient.encrypt(symmetricKey);

      let encryptionModuleReceiver = await AsymmetricModuleFactory.createRsaModule(cryptIDRecipient.publicKey);
      symmetricKeyReceiver = await encryptionModuleReceiver.encrypt(symmetricKey);
    }

    const body = new BlockBase(
      chainID,
      blockID,
      cryptIDSender,
      cryptIDRecipient,
      type,
      keyAlgorithm,
      blockPayload,
      symmetricKeySender,
      symmetricKeyReceiver,
      iv) //base64 encoded payload

    body.signedHashSender = await this.serviceConfiguration.signingModuleClient.sign(body.getJcsPlain());

    return new Promise<Response<string>>((resolve) => {
      return this.httpClient.call(requestOptions, false, body).then(() => {
        let response = new Response<string>();
        response.data = blockID;
        resolve(response);
      });
    });
  }

  async getBlockById(chainID: string, blockID: string): Promise<Response<Block[]>>{
    const requestOptions: RequestOptions = this.createRequestOptions('/chains/' + chainID + '/blocks/' + blockID, Method.GET, this.serviceConfiguration.session.token);
    const response =  await this.httpClient.call(requestOptions, true);
    response.data = this.buildBlockObjects(response.data);
    return response;
  }

  async getNewestBlock(chainID: string): Promise<Response<Block[]>>{
    const requestOptions: RequestOptions = this.createRequestOptions('/chains/' + chainID + '/blocks/newest', Method.GET, this.serviceConfiguration.session.token);
    const response = await this.httpClient.call(requestOptions, true);
    response.data = this.buildBlockObjects(response.data);
    return response;
  }

  async getBlocks(chainID: string, searchParamsBlock: SearchParamsBlock): Promise<Response<Block[]>>{
    const params = qs.stringify(searchParamsBlock);
    const requestOptions: RequestOptions = this.createRequestOptions('/chains/' + chainID + '/blocks?' + params, Method.GET, this.serviceConfiguration.session.token);
    const response = await this.httpClient.call(requestOptions, true);
    response.data = this.buildBlockObjects(response.data);
    return response;
  }
}
