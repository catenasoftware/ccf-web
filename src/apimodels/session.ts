export default class Session{
  token: string
  start: string
  expiration: string

  isValid(): boolean{
    const dateExpiration = new Date(this.expiration);
    const currentDateLimit = new Date();
    currentDateLimit.setMinutes(currentDateLimit.getMinutes() + 1);
    return currentDateLimit < dateExpiration;
  }
}
