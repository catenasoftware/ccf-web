const jcs = require('json-canonicalize')

export default class UserBase{
  id: string;
  publicKey: string;
  name: string;
  email: string;
  additionalData: string;

  getJcsPlain(): string{
    return jcs.canonicalizeEx(this, {exclude:[
        'name',
        'email',
        'additionalData'
      ]});
  }
}
