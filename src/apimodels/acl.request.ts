import {AclEntryRequest} from "./acl.entry.request";

export class AclRequest{
  chainId: string
  aclEntryRequests: AclEntryRequest[]
}
