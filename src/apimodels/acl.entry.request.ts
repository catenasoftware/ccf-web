import {AclEntryBase} from "./acl.entry.base";

export class AclEntryRequest extends AclEntryBase{
  delete: boolean;

  constructor(publicKeyAclTarget: string, read: boolean, write: boolean, deleteEntry: boolean = false) {
    super(publicKeyAclTarget, read, write);
    this.delete = deleteEntry;
  }
}
