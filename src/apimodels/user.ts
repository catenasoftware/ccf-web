import UserBase from "./user.base";


export default class User extends UserBase{

  chainID: string;
  blockID: string;
  created: Date;
  modified: Date;
}
