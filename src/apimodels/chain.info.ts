import ChainInfoBase from "./chain.info.base";

export default class ChainInfo extends ChainInfoBase{
  created: string
  modified: string
  blockID: string
}
