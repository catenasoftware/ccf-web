import ChainInfoBase from "./chain.info.base";

export default class ChainInfoRequest extends ChainInfoBase{
  subscriptionID: string
}
