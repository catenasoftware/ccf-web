export class AclEntryBase{
  publicKey: string
  read: boolean
  write: boolean

  constructor(publicKeyAclTarget: string, read: boolean, write: boolean) {
    this.publicKey = publicKeyAclTarget;
    this.read = read;
    this.write = write;
  }
}
