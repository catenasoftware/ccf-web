import {KeyAlgorithm} from "../crypto/asymmetric.module";
import {AsymmetricModuleFactory} from "../crypto/asymmetric.module.factory";
/**
 * The cryptId is a signature, where the ciphertext is the signed public key
 */
export class Signature{
  cipherText: string //must be base64 encoded
  algorithm: KeyAlgorithm
  publicKey: string //must be base64 encoded

  constructor(cipherText: string, algorithm: KeyAlgorithm, publicKey: string) {
    this.cipherText = cipherText;
    this.algorithm = algorithm;
    this.publicKey = publicKey;
  }

  async verifySignature(): Promise<boolean>{
    const signingModule = await AsymmetricModuleFactory.createAsymmetricModule(this.algorithm, this.publicKey);
    return await signingModule.verify(atob(this.publicKey), this.cipherText);
  }
}
export default Signature;
