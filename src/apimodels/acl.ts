import ACLEntry from "./acl.entry";

export default class ACL{
  chainID: string
  publicRead: boolean
  publicWrite: boolean
  closed: boolean
  aclEntries: ACLEntry[]
}


