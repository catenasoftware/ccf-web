import Signature from "./signature";
const jcs = require('json-canonicalize')

export default class BlockBase{
  chainID: string
  blockID: string
  cryptIDSender: Signature
  cryptIDRecipient: Signature
  type: number
  symmetricKeySender: string = null
  symmetricKeyRecipient: string = null
  initialisationVector: string = null
  algorithm: string
  payload: string //must be base64 encoded
  signedHashSender: Signature = null

  constructor( chainID: string,
               blockID: string,
               cryptIDSender: Signature,
               cryptIDRecipient: Signature,
               type: number,
               algorithm: string,
               payload: string,
               symmetricKeySender?: string,
               symmetricKeyRecipient?: string,
               initialisationVector?: string,
  ) {
    this.chainID = chainID;
    this.blockID = blockID;
    this.cryptIDSender = cryptIDSender;
    this.cryptIDRecipient = cryptIDRecipient;
    this.type = type;
    this.symmetricKeySender = symmetricKeySender;
    this.symmetricKeyRecipient = symmetricKeyRecipient;
    this.initialisationVector = initialisationVector;
    this.algorithm = algorithm;
    this.payload = payload;
  }

  payloadDecoded(){
    return atob(this.payload);
  }

  /*
   * Create jcs from TransactionBase and exclude all fields which are null in case of
   *  an unencrypted transaction
   */
  getJcsPlain(): string{
    let excludes = ['signedHashSender'];

    if(this.symmetricKeySender == null){
      excludes.push('symmetricKeySender');
    }

    if(this.initialisationVector == null){
      excludes.push('initialisationVector');
    }

    if(this.symmetricKeyRecipient == null){
      excludes.push('symmetricKeyRecipient');
    }

    return jcs.canonicalizeEx(this, {exclude:excludes});
  }

}
