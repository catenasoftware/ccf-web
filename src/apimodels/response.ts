import Notes from "./notes";
import Signature from "./signature";
import {AsymmetricModuleFactory} from "../crypto/asymmetric.module.factory";
const con = require('json-canonicalize')



export class Response<T>{
  /**
    Response-Object
   */
  data: T
  type: string
  /**
   CCM-Signature
   */
  signature: Signature
  note: Notes
  /**
   * Indicates whether the CCM call was successful (2XX http status)
   */
  isOk: boolean
  /**
    HTTP Error in case isOk is false
  */
  error?: Error;
  /**
      HTTP status code in case there is an error
   */
  statusCode?: number;

  constructor(isOk: boolean = true, error? : Error, statusCode?: number) {
    this.isOk = isOk;
    this.error = error;
    this.statusCode = statusCode;
  }

  async  verifySignature(): Promise<boolean>{
    const signingModule = await AsymmetricModuleFactory.createAsymmetricModule(this.signature.algorithm, this.signature.publicKey);
    return await signingModule.verify(con.canonicalize(this.data), this.signature.cipherText);
  }

}
