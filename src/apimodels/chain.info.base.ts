import Signature from "./signature";

export default class ChainInfoBase{
  id: string
  name: string
  description: string
  closed: boolean
  publicRead: boolean
  publicWrite: boolean
  cryptIDOwner: Signature
}
