import {AclEntryBase} from "./acl.entry.base";

export default class ACLEntry extends AclEntryBase{
  transactionID: string
  created: Date
  modified: Date
}
