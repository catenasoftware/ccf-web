import Signature from "./signature";

export default class CreationRequest{
  subscriptionID: string
  signature: Signature
}
