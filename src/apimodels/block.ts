import BlockBase from "./blockBase";
import Signature from "./signature";

export default class Block extends BlockBase{
  predecessorID: string
  predecessorSignedHashCCM: string
  signedHashCCM: Signature
  created: string
}
