import {TestConfiguration} from "../../test.configuration/test.configuration";
import CCF from "../../ccf";

describe("Test chain interfaces of the ccf", () => {

  let alice: CCF;
  let bob: CCF;

  beforeEach(async () => {
    const aliceKeys = await CCF.generateRsaKeyPairDerFormat();
    alice = await CCF.createCCFWithRsaKeys(TestConfiguration.host, TestConfiguration.port, aliceKeys, TestConfiguration.subscriptionID);

    const bobKeys = await CCF.generateRsaKeyPairDerFormat();
    bob = await CCF.createCCFWithRsaKeys(TestConfiguration.host, TestConfiguration.port, bobKeys, TestConfiguration.subscriptionID);
  });
  /*
    createChain
   */
  it("Test createChain positive",   async () => {
    const chainInfo = await alice.createChain();
    expect(chainInfo.isOk).toBe(true);
    expect(chainInfo.data.length > 0).toBe(true);
  });
  /*
    getMyChains
  */
  it("Test getMyChains positive not empty",   async () => {
    const chainInfo = await alice.createChain();
    expect(chainInfo.isOk).toBe(true);

    const myChains = await alice.getMyChains();
    expect(myChains.isOk).toBe(true);
    expect(chainInfo.data[0]).toStrictEqual(myChains.data[0]);
  });
  it("Test getMyChains positive empty",   async () => {
    const myChains = await alice.getMyChains();
    expect(myChains.isOk).toBe(true);
    expect(myChains.data.length).toBe(0);
  });
  /*
      getChainById
  */
  it("Test getChainById positive not empty",   async () => {
    const chainInfo = await alice.createChain();

    const myChains = await alice.getChainById(chainInfo.data[0].id);
    expect(myChains.isOk).toBe(true);
    expect(myChains.data.length).toBe(1);
  });
  it("Test getChainById negative",   async () => {
    const chainInfo = await alice.createChain();

    const myChains = await bob.getChainById(chainInfo.data[0].id);
    expect(myChains.isOk).toBe(false);
  });
  /*
    updateChain
  */
  it("Test updateChain positive",   async () => {
    const chainInfo = await alice.createChain();

    const chainInfoCreated = chainInfo.data[0];

    chainInfoCreated.name = new Date().toString() + 'new_name';
    chainInfoCreated.description = new Date().toString() + 'new_description';
    chainInfoCreated.closed = false;
    chainInfoCreated.publicRead = true;
    chainInfoCreated.publicWrite = true;

    const updateResult = await alice.updateChain(chainInfoCreated);
    expect(updateResult.isOk).toBe(true);
    const chainByIdAfterUpdate = await alice.getChainById(chainInfoCreated.id);
    /*
        Check that the chain was updated properly
     */
    expect(chainByIdAfterUpdate.isOk).toBe(true);
    expect(chainByIdAfterUpdate.data[0].name).toBe(chainInfoCreated.name);
    expect(chainByIdAfterUpdate.data[0].description).toBe(chainInfoCreated.description);
    expect(chainByIdAfterUpdate.data[0].closed).toBe(chainInfoCreated.closed);
    expect(chainByIdAfterUpdate.data[0].publicRead).toBe(chainInfoCreated.publicRead);
    expect(chainByIdAfterUpdate.data[0].publicWrite).toBe(chainInfoCreated.publicWrite);
  });
  /*
      getChains with filters:
       - ownerPublicKey
       - nameContains
       - descriptionContains
  */
  it("Test getChains with filter ownerPublicKey",   async () => {
    /*
        Create 4 chains which should be returned later when calling getChains with ownerPublicKey filter
        of the current user
     */
    await alice.createChain();
    await alice.createChain();
    await alice.createChain();
    await alice.createChain();
    const getChainsResponse = await alice.getChains({
      ownerPublicKey: alice.getMyPublicKey()
    });
    expect(getChainsResponse.isOk).toBe(true);
    expect(getChainsResponse.data.length).toBe(4);
  });
  it("Test getChains with filter nameContains",   async () => {

    const chainCreated = (await alice.createChain()).data[0];

    chainCreated.name = Date.now() + 'new_date';
    await alice.updateChain(chainCreated)
    const getChainsResponse = await alice.getChains({
      nameContains: chainCreated.name
    });
    expect(getChainsResponse.isOk).toBe(true);
    expect(getChainsResponse.data.length).toBe(1);
  });
  it("Test getChains with filter descriptionContains",   async () => {

    const chainCreated = (await alice.createChain()).data[0];

    chainCreated.description = Date.now() + 'new_date';
    await alice.updateChain(chainCreated)
    const getChainsResponse = await alice.getChains({
      descriptionContains: chainCreated.description
    });
    expect(getChainsResponse.isOk).toBe(true);
    expect(getChainsResponse.data.length).toBe(1);
  });
});
