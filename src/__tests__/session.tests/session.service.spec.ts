import SessionService from "../../service/session.service";
import {TestConfiguration} from "../../test.configuration/test.configuration";
import Session from "../../apimodels/session";

describe("Given an instance of my session service", () => {

      /*
       *  Test createNewConnectToken
       */
    it("Test createNewConnectToken",   async () => {
      const serviceConfiguration = await new TestConfiguration().getServiceConfigurationWithoutSession();
      const sessionService = new SessionService(serviceConfiguration);

      const response = await sessionService.createNewConnectToken();
      expect(response.isOk).toBeTruthy();
    });
    it("Test createNewSessionResponse",   async () => {
      const serviceConfiguration = await new TestConfiguration().getServiceConfigurationWithoutSession();
      const sessionService = new SessionService(serviceConfiguration);

      const response = await sessionService.createNewSessionResponse();
      expect(response.isOk).toBeTruthy();
    });
  /*
   *  Test createNewSession
   */
  it("Test createNewSession",   async () => {
    const serviceConfiguration = await new TestConfiguration().getServiceConfigurationWithoutSession();
    const sessionService = new SessionService(serviceConfiguration);

    const response = await sessionService.createNewSession();
    expect(response.isOk).toBeTruthy();
  });
  it("Test expired SessionToken",   async () => {
    const expiredSession = {
      token: '7746cb1a-9d00-474f-a4c0-1fdaf2a84c33',
      start: '2021-02-21T14:28:43.062265Z',
      expiration: '2021-02-21T14:38:43.058191Z'
    }

    const expiredSessionObject: Session = Object.setPrototypeOf(expiredSession, Session.prototype);
    expect(expiredSessionObject.isValid()).toBeFalsy();
  });
  /*
   *  Test getCryptIdCCM
   */
  it("Test getCryptIdCCM",   async () => {
    const serviceConfiguration = await new TestConfiguration().getServiceConfigurationWithoutSession();
    const sessionService = new SessionService(serviceConfiguration);

    const cryptId = await sessionService.getCryptIdCCMFromCCM();
    expect(cryptId.isOk).toBeTruthy();
  });
});
