import AESService from "../crypto/aes";
import RsaModule from "../crypto/rsa.module";
import {AsymmetricModuleFactory} from "../crypto/asymmetric.module.factory";
import {KeyAlgorithm} from "../crypto/asymmetric.module";

describe("Given an instance of my AES and RSA Service", () => {
  describe("Test encryption and combination of RSA and AES encryption", () => {
    it("Test AES encryption and decryption",  async () => {

      let aes = new AESService();

      let message = 'C_Chain!';
      let doEncrypt = btoa(message);

      let encrypted = await aes.encrypt(doEncrypt);

      let decrypted = await aes.decrypt(encrypted);

      expect(message).toBe(atob(decrypted));

    });
    it("Test RSA encyption/decryption of AES key",  async () => {

      let rsaKeys = await RsaModule.generateKeyPairDer();
      let rsa = await new RsaModule().importKeys(rsaKeys);
      let aes = new AESService();


      let message = 'C_Chain!';
      let doEncrypt = btoa(message);
      let encryptionData = await aes.encrypt(doEncrypt);

      let encryptionKey = encryptionData.symmetricKey;

      let encryptedKey = await rsa.encrypt(encryptionKey);
      encryptionData.symmetricKey  = await rsa.decrypt(encryptedKey);

      expect(message).toBe(atob(await aes.decrypt(encryptionData)));

    });
    it("Test send encrypted message",  async () => {


      let aliceKeys = await RsaModule.generateKeyPairDer();
      let alice = await new RsaModule().importKeys(aliceKeys);
      let alicePublicKeyPlain = aliceKeys.publicKey.key;
      let aliceAsymmetricEncryptionModule =  await AsymmetricModuleFactory.createRsaModule(alicePublicKeyPlain);

      let message = 'hallo';
      let aes = new AESService();
      let encryptedData = await aes.encrypt(btoa(message));

      let encryptedSymmetricKey =  await aliceAsymmetricEncryptionModule.encrypt(encryptedData.symmetricKey);


      let decryptedSymmetricKey = await alice.decrypt(encryptedSymmetricKey);
      expect(decryptedSymmetricKey).toBe(encryptedData.symmetricKey);

      encryptedData.symmetricKey = decryptedSymmetricKey;
      let decryptedData = await aes.decrypt(encryptedData);
      expect(message).toBe(atob(decryptedData));
    });
  });
});
