import RsaModule from "../../crypto/rsa.module";
import CCF from "../../ccf";

describe("Given an instance of RSA Module", () => {
    it("Test signature sign and veify",  async () => {

     const keyPair = await CCF.generateRsaKeyPairDerFormat();
     // console.log(keyPair);
     const rsaModule = await new RsaModule().importKeys(keyPair);

     const data = 'Hello World!';
     const signature = await rsaModule.sign(data);
     expect(rsaModule.verify(data, signature.cipherText)).toBeTruthy();
    });
});
