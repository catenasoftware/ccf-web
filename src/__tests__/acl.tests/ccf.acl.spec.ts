import {TestConfiguration} from "../../test.configuration/test.configuration";
import CCF from "../../ccf";
import {AclEntryRequest} from "../../apimodels/acl.entry.request";


describe("Test acl interfaces of the ccf", () => {

  let alice: CCF;
  let bob: CCF;
  let eve: CCF;
  let chainId: string;

  beforeEach(async () => {
    const aliceKeys = await CCF.generateRsaKeyPairDerFormat();
    alice = await CCF.createCCFWithRsaKeys(TestConfiguration.host, TestConfiguration.port, aliceKeys, TestConfiguration.subscriptionID);
    chainId = (await alice.createChain()).data[0].id;

    const bobKeys = await CCF.generateRsaKeyPairDerFormat();
    bob = await CCF.createCCFWithRsaKeys(TestConfiguration.host, TestConfiguration.port, bobKeys, TestConfiguration.subscriptionID);

    const eveKeys = await CCF.generateRsaKeyPairDerFormat();
    eve = await CCF.createCCFWithRsaKeys(TestConfiguration.host, TestConfiguration.port, eveKeys, TestConfiguration.subscriptionID);
  });
  /*
   *    ccf.createOrUpdateAcl
   */
  it("Test create acl positive",   async () => {
      const createdAcl = await alice.createOrUpdateAclEntry(chainId, new AclEntryRequest(bob.getMyPublicKey(), true, true));
      expect(createdAcl.isOk).toBe(true);
  });
  it("Test create acl negative",   async () => {
    const createdAcl = await bob.createOrUpdateAclEntry(chainId, new AclEntryRequest(bob.getMyPublicKey(), true, true));
    expect(createdAcl.isOk).toBe(false);
  });
  it("Test update acl positive",   async () => {
    const createdAcl1 = await alice.createOrUpdateAclEntry(chainId, new AclEntryRequest(bob.getMyPublicKey(), true, true));
    const createdAcl2 = await alice.createOrUpdateAclEntry(chainId, new AclEntryRequest(bob.getMyPublicKey(), true, false));
    expect(createdAcl1.isOk).toBe(true);
    expect(createdAcl2.isOk).toBe(true);

    const allAcl = await alice.getAcl(chainId);
    expect(allAcl.isOk).toBe(true);
    expect(allAcl.data.aclEntries.length).toBe(1);
  });
  /*
   *    ccf.getAllAcl
   */
  it("Test getAllAcl empty entires positive",   async () => {
    const allAclEmpty = await alice.getAcl(chainId);
    expect(allAclEmpty.isOk).toBe(true);
    expect(allAclEmpty.data.aclEntries).toBe(undefined);
  });
  it("Test getAllAcl positive",   async () => {
    await alice.createOrUpdateAclEntry(chainId, new AclEntryRequest(bob.getMyPublicKey(), true, true));
    await alice.createOrUpdateAclEntry(chainId, new AclEntryRequest(eve.getMyPublicKey(), true, true));
    const allAcl = await alice.getAcl(chainId);
    expect(allAcl.isOk).toBe(true);
    expect(allAcl.data.aclEntries.length).toBe(2);
  });
  /*
   *    ccf.getAclByPublicKey
   */
  it("Test getAclByPublicKey positive not empty",   async () => {
    await alice.createOrUpdateAclEntry(chainId, new AclEntryRequest(bob.getMyPublicKey(), true, true));
    const allAcl = await alice.getAclByPublicKey(chainId, bob.getMyPublicKey());
    expect(allAcl.isOk).toBe(true);
    expect(allAcl.data.aclEntries.length).toBe(1);
  });
  it("Test getAclByPublicKey positive empty",   async () => {
    const allAcl = await alice.getAclByPublicKey(chainId, bob.getMyPublicKey());
    expect(allAcl.isOk).toBe(true);
  });
  it("Test getAclByPublicKey negative",   async () => {
    const allAcl = await bob.getAcl(chainId);
    expect(allAcl.isOk).toBe(false);
  });
  /*
   *  ccf.deleteAcl
   */
  it("Test deleteAcl positive not empty",   async () => {
    await alice.createOrUpdateAclEntry(chainId, new AclEntryRequest(bob.getMyPublicKey(), true, true));
    const allAcl = await alice.deleteAclEntry(chainId, bob.getMyPublicKey());
    expect(allAcl.isOk).toBe(true);
  });
  it("Test deleteAcl positive empty",   async () => {
    const allAcl = await alice.deleteAclEntry(chainId, bob.getMyPublicKey());
    expect(allAcl.isOk).toBe(true);
  });
  it("Test deleteAcl negative",   async () => {
    const allAcl = await bob.deleteAclEntry(chainId, bob.getMyPublicKey());
    expect(allAcl.isOk).toBe(false);
  });
});
