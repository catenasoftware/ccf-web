import {TestConfiguration} from "../../test.configuration/test.configuration";
import CCF from "../../ccf";

describe("Test block interfaces of the ccf", () => {

  let alice: CCF;
  let chainId: string;

  beforeEach(async () => {
    const aliceKeys = await CCF.generateRsaKeyPairDerFormat();
    alice = await CCF.createCCFWithRsaKeys(TestConfiguration.host, TestConfiguration.port, aliceKeys, TestConfiguration.subscriptionID);
    chainId = (await alice.createChain()).data[0].id;
  });
  it("Test createBlock positive 1",   async () => {
    const createdBlock = await alice.createBlock(chainId, alice.getMyCryptId(), 30, "Hallo Welt!");
    expect(createdBlock.isOk).toBe(true);
  });
  it("Test createBlock with encrypted payload",   async () => {
    const message = "Hello Catena!"
    const createdBlock = await alice.createBlock(chainId, alice.getMyCryptId(), 30, message, true);
    const getBlockById = await alice.getBlockById(chainId, createdBlock.data);
    expect(createdBlock.isOk).toBe(true);

    const payloadDecrypted = await alice.getPayloadDecrypted(getBlockById.data[0]);
    expect(atob(payloadDecrypted)).toBe(message);

  });
  it("Test createBlock positive 2",   async () => {
    const payload = "Hallo Welt!";
    const createdBlock = await alice.createBlock(chainId, alice.getMyCryptId(), 30, payload);
    const getBlockById = await alice.getBlockById(chainId, createdBlock.data);
    expect(getBlockById.isOk).toBe(true);
    expect(getBlockById.data[0].payloadDecoded()).toBe(payload);
  });
  it("Test createBlock negative",   async () => {
    const payload = "Hallo Welt!";
    await alice.createBlock(chainId, alice.getMyCryptId(), 30, payload);
    const getBlockById = await alice.getBlockById(chainId, chainId);
    expect(getBlockById.isOk).toBe(false);
  });
  it("Test getAllBlock of a chain positive",   async () => {
    /*
     *    Create blocks
     */
    const payload = 'Hallo Welt!'
    await alice.createBlock(chainId, alice.getMyCryptId(), 30, payload);
    await alice.createBlock(chainId, alice.getMyCryptId(), 30, payload);
    await alice.createBlock(chainId, alice.getMyCryptId(), 31, payload);
    await alice.createBlock(chainId, alice.getMyCryptId(), 32, payload);
    await alice.createBlock(chainId, alice.getMyCryptId(), 33, payload);
    /*
     *    Test getAllBlocks
     */
    const getAllBlocks = await alice.getAllBlocks(chainId);
    expect(getAllBlocks.isOk).toBe(true);
    // Expect 5 + 1 (First block is chain creation block) blocks
    expect(getAllBlocks.data.length).toBe(6);
  });
  it("Test getAllBlock of a chain negative",   async () => {
    const bobKeys = await CCF.generateRsaKeyPairDerFormat();
    const bob = await CCF.createCCFWithRsaKeys(TestConfiguration.host, TestConfiguration.port, bobKeys, TestConfiguration.subscriptionID);
    const getAllBlocks = await bob.getAllBlocks(chainId);
    expect(getAllBlocks.isOk).toBe(false);
  });
  /*
   *    Test getNewestBlock
   */
  it("Test getNewestBlock positive",   async () => {

    const payload = "Hallo Welt";
    const type2 = 22
    const blockID1 = await alice.createBlock(chainId, alice.getMyCryptId(), 20, payload);
    const blockID2 = await alice.createBlock(chainId, alice.getMyCryptId(), type2, payload);

    expect(blockID1.isOk).toBe(true);
    expect(blockID2.isOk).toBe(true);

    const response = await alice.getNewestBlock(chainId);
    expect(response.data[0].blockID).toBe(blockID2.data);
  });
  it("Test getNewestBlock negative",   async () => {
    const bobKeys = await CCF.generateRsaKeyPairDerFormat();
    const bob = await CCF.createCCFWithRsaKeys(TestConfiguration.host, TestConfiguration.port, bobKeys, TestConfiguration.subscriptionID);

    const response = await bob.getNewestBlock(chainId);
    expect(response.isOk).toBe(false);
  });
  /*
   *    Test getBlocks
   */
  it("Test getBlocks without filter positive",   async () => {
    await alice.createBlock(chainId, alice.getMyCryptId(), 20, 'Hello World!');
    const response = await alice.getBlocks(chainId, {});
    expect(response.isOk).toBe(true);
    expect(response.data.length).toBe(2);
  });
  /*
   *    Test getBlocks with filter
   *    - typeId
   *    - senderPublicKey
   *    - recipientPublicKey
   */
  it("Test getBlocks with filter typeId positive",   async () => {
    await alice.createBlock(chainId, alice.getMyCryptId(), 30, 'Hello World!');
    await alice.createBlock(chainId, alice.getMyCryptId(), 31, 'Hello World!');
    await alice.createBlock(chainId, alice.getMyCryptId(), 31, 'Hello World!');
    const response = await alice.getBlocks(chainId, {
      typeID: 31
    });
    expect(response.isOk).toBe(true);
    expect(response.data.length).toBe(2);
  });
  it("Test getBlocks with filter senderPublicKey positive",   async () => {
    await alice.createBlock(chainId, alice.getMyCryptId(), 30, 'Hello World!');
    await alice.createBlock(chainId, alice.getMyCryptId(), 31, 'Hello World!');
    await alice.createBlock(chainId, alice.getMyCryptId(), 31, 'Hello World!');
    // Get blocks created by alice (3 above)
    const response = await alice.getBlocks(chainId, {
      senderPublicKey: alice.getMyPublicKey()
    });
    expect(response.isOk).toBe(true);
    expect(response.data.length).toBe(3);
    // Get blocks created by ccm (1, the initial block)
    const response2 = await alice.getBlocks(chainId, {
      senderPublicKey: alice.getCryptIdCCM().publicKey
    });
    expect(response2.isOk).toBe(true);
    expect(response2.data.length).toBe(1);
  });
  it("Test getBlocks with filter recipientPublicKey positive",   async () => {
    await alice.createBlock(chainId,  alice.getCryptIdCCM(), 30, 'Hello World!');
    await alice.createBlock(chainId, alice.getMyCryptId(), 31, 'Hello World!');
    await alice.createBlock(chainId, alice.getMyCryptId(), 31, 'Hello World!');
    // Get blocks created by alice (3 above)
    const response = await alice.getBlocks(chainId, {
      recipientPublicKey: alice.getMyPublicKey()
    });
    expect(response.isOk).toBe(true);
    expect(response.data.length).toBe(2);
    // Get blocks created by ccm (1, the initial block)
    const response2 = await alice.getBlocks(chainId, {
      recipientPublicKey: alice.getCryptIdCCM().publicKey
    });
    expect(response2.isOk).toBe(true);
    expect(response2.data.length).toBe(2);
  });
});
