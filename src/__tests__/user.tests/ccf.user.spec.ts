import {TestConfiguration} from "../../test.configuration/test.configuration";
import CCF from "../../ccf";

describe("Given an instance of user service", () => {
  let alice: CCF;

  beforeEach(async () => {
    const aliceKeys = await CCF.generateRsaKeyPairDerFormat();
    alice = await CCF.createCCFWithRsaKeys(TestConfiguration.host, TestConfiguration.port, aliceKeys, TestConfiguration.subscriptionID);
  });
  /*
    Test createUser
  */
  it("Test createUser",   async () => {
    const res = await alice.createUser('alice');
    expect(res.isOk).toBeTruthy();
    expect(true);
  }, 20000);
  /*
      Test getUserByPublicKey
   */
  it("Test getUserByPublicKey",   async () => {
    await alice.createUser('alice');

    const res = await alice.getUserByPublicKey(alice.getMyPublicKey());
    expect(res.isOk).toBe(true);
  }, 20000);
  /*
    Test getUserById
 */
  it("Test getUserById",   async () => {
    await alice.createUser('alice');
    const user = await alice.getMyUser();
    const res = await alice.getUserById(user.data[0].id);
    expect(res.isOk).toBe(true);
    expect(res.data.length > 0).toBeTruthy();
  }, 20000);
  /*
    Test getMyUser
  */
  it("Test getMyUser",   async () => {
    await alice.createUser('alice');
    const user = await alice.getMyUser();
    expect(user.isOk).toBe(true);
    expect(user.data[0].name).toBe('alice');
  }, 20000);
  /*
      Test updateUser
  */
  it("Test updateUser",   async () => {
    await alice.createUser('alice');
    var res = await alice.updateUser('catena', 'catena@software.de');
    expect(res.isOk).toBeTruthy();
    const updatedUser = await alice.getMyUser();
    expect(updatedUser.data[0].name).toBe('catena');
  }, 20000);
  /*
      Test deleteUser
  */
  it("Test deleteUser positive",   async () => {
    await alice.createUser('alice');
    const res = await alice.deleteMyUser();
    expect(res.isOk).toBeTruthy();
    const myUser = await alice.getMyUser();
    expect(myUser.isOk).toBe(true);
    expect(myUser.data.length == 0).toBe(true);
  }, 20000);
  /*
    Test deleteUser
  */
  it("Test deleteUser without user",   async () => {
    const myUser = await alice.deleteMyUser();
    expect(myUser.isOk).toBe(true);
  }, 20000);
  /*
      Test getUsers with parameters
      - UUID?: string,
      - PublicKey?: string,
      - NameContains?: string,
      - EmailContains?: boolean,
      - FromDate?: Date,
  */
  it("Test getUsers with parameter publicKey",   async () => {
    await alice.createUser('alice__');
    const res = await alice.getUsers(
      {publicKey: alice.getMyPublicKey()}
    );
    expect(res.isOk).toBe(true);
    expect(res.data[0].name).toBe('alice__');
  }, 20000);
  it("Test getUsers with parameter uuid",   async () => {
    await alice.createUser('alice__');
    const myUser = await alice.getMyUser();
    const res = await alice.getUsers(
      {UUID: myUser.data[0].id}
    );
    expect(res.isOk).toBe(true);
    expect(res.data[0].name).toBe('alice__');
  }, 20000);
  it("Test getUsers with parameter nameContains",   async () => {
    const name = new Date().toString() + 'alice___'
    await alice.createUser(name);
    const myUser = await alice.getMyUser();
    const res = await alice.getUsers(
      {nameContains: name}
    );
    expect(res.isOk).toBe(true);
    expect(res.data[0].name).toBe(name);
    expect(res.data[0].publicKey).toBe(myUser.data[0].publicKey);
  }, 20000);
  it("Test getUsers with parameter emailContains",   async () => {
    const email = new Date().toString() + 'email__alice___'
    await alice.createUser('alice', email);
    const myUser = await alice.getMyUser();
    const res = await alice.getUsers(
      {emailContains: email}
    );
    expect(res.isOk).toBe(true);
    expect(res.data[0].email).toBe(email);
    expect(res.data[0].publicKey).toBe(myUser.data[0].publicKey);
  }, 20000);
  it("Test getUsers with parameter fromDate",   async () => {
    const date = new Date();
    await alice.createUser('alice');
    const myUser = await alice.getMyUser();
    const res = await alice.getUsers(
      {fromDate: date}
    );
    expect(res.isOk).toBe(true);
    expect(res.data[0].publicKey).toBe(myUser.data[0].publicKey);
  }, 20000);
});
