import ServiceConfiguration from "./service/service.configuration";
import SessionService from "./service/session.service";
import {ChainService} from "./service/chain.service";
import BlockService from "./service/block.service";
import {SearchParamsChain} from "./http/search.params.chain";
import {SearchParamsBlock} from "./http/search.params.block";
import Signature from "./apimodels/signature";
import {Response} from "./apimodels/response";
import {AclEntryRequest} from "./apimodels/acl.entry.request";
import AclService from "./service/acl.service";
import Block from "./apimodels/block";
import RsaModule from "./crypto/rsa.module";
import CCFRsaKeyPair from "./crypto/rsa_key";
import AsymmetricModule from "./crypto/asymmetric.module";
import ChainInfo from "./apimodels/chain.info";
import UserService from "./service/user.service";
import User from "./apimodels/user";
import UserBase from "./apimodels/user.base";
import {SearchParamsUser} from "./http/search.params.user";
import ACL from "./apimodels/acl";
import ACLEntry from "./apimodels/acl.entry";
import {CCFRsaKey} from "./crypto/rsa_key";
import {CatenaKeyFormat} from "./crypto/rsa_key";
import {CatenaKeyType} from "./crypto/rsa_key";
import BlockBase from "./apimodels/blockBase";
import ChainInfoBase from "./apimodels/chain.info.base";
import {KeyAlgorithm} from "./crypto/asymmetric.module"
import Notes from  "./apimodels/notes";
import AESService from "./crypto/aes";
import EncryptionData from "./crypto/encryption.data";

/**
 * Default class which provides all functions to communicate with the CCM.
 *
 * ```typescript
 *
 *   // Code example: initialize the ccf to use the CCM hosted by Catena GmbH
 *   // and personal subscriptionID '00000000-0000-0000-0000-0000000000000'
 *
 * const keyPair = await CCF.generateRsaKeyPairDerFormat();
 * const ccf = await CCF.createCCFWithRsaKeys('api.cchain.prod.catena.software', 443, keyPair, '00000000-0000-0000-0000-0000000000000');
 * ```
 */
export class CCF {

  /** @hidden */
  private serviceConfiguration: ServiceConfiguration;

  /** @hidden */
  private constructor(host: string, port: number, asymmetricModule: AsymmetricModule, subscriptionID: string) {
    this.serviceConfiguration = new ServiceConfiguration(host, port, asymmetricModule, true, subscriptionID);
  }

  /**
   * @param host  Host of the CCM (e.g 'localhost' if the CCM is running on localhost
   * @param port Port of the CCM
   * @param ccfRsaKeyPair Can be generated {@link generateRsaKeyPairDerFormat} or created by the application with key material from any other source
   * @param subscriptionID  Individual ID for access and payment
   */
  static async createCCFWithRsaKeys(host: string, port: number, ccfRsaKeyPair: CCFRsaKeyPair, subscriptionID: string): Promise<CCF>{
    const asymmetricModule = await new RsaModule().importKeys(ccfRsaKeyPair);
    return new CCF(host,port,asymmetricModule, subscriptionID);
  }

  /**
   *
   * Helper function to easily generate RSA-Key-Pair for initializing {@link CCF} instance.
   *
   * @param modulusLength  Modulus Length of the generated RSA Key (Optional).
   */
  static async generateRsaKeyPairDerFormat(modulusLength: number = 1024): Promise<CCFRsaKeyPair>{
    return await RsaModule.generateKeyPairDer( modulusLength);
  }

  /** @hidden */
  private async getConfigurationWithSession(): Promise<ServiceConfiguration> {
    if(!this.serviceConfiguration.session || !this.serviceConfiguration.session.isValid()){
      let sessionResponse = await new SessionService(this.serviceConfiguration).createNewSession();
      if(sessionResponse.isOk){
        this.serviceConfiguration.session = sessionResponse.data;
      }
    }
    if(!this.serviceConfiguration.cryptIdCCM){
      let cryptIdCCMResponse = await new SessionService(this.serviceConfiguration).getCryptIdCCMFromCCM();
      if(cryptIdCCMResponse.isOk && cryptIdCCMResponse.data.length > 0){
        this.serviceConfiguration.cryptIdCCM = cryptIdCCMResponse.data[0];
      }
    }
    return this.serviceConfiguration;
  }

  /**
   * Returns the cryptId of the keys which belongs to the current {@link CCF} instance
   */
  getMyCryptId(): Signature{
    return this.serviceConfiguration.signingModuleClient.getCryptId();
  }

  /**
   * Returns the publicKey which belongs to the current {@link CCF} instance
   * @returns base64-string in DER format
   */
  getMyPublicKey(): string{
    return this.getMyCryptId().publicKey;
  }

  /**
   * tested @ccf.chain.spec
   *
   * Returns the cryptId of the CCM
   */
   getCryptIdCCM(): Signature {
    return this.serviceConfiguration.cryptIdCCM;
  }

  /**
   * tested @ccf.chain.spec
   *
   * Returns the KeyPair which was used to initialize the current {@link CCF} instance
   */
  async getKeyPair(): Promise<CCFRsaKeyPair> {
    return await this.serviceConfiguration.signingModuleClient.exportKeys();
  }

  // ------------------------------------------------------------------------------------------------------------------------
  // Chains
  // ------------------------------------------------------------------------------------------------------------------------

  /** @hidden */
  private async getChainService(): Promise<ChainService> {
    return new ChainService(await this.getConfigurationWithSession());
  }

  /**
   * @returns Response.data contains the created chain if successful
   */
  async createChain(): Promise<Response<ChainInfo[]>> {
    const chainService = await this.getChainService()
    return chainService.createChain();
  }

  /*
   * tested @ccf.chain.spec
   *
   * @param chainId
   *
   */
  async getChainById(chainId: string): Promise<Response<ChainInfo[]>>{
    return (await this.getChainService()).getChainById(chainId);
  }

  /**
   *  tested @ccf.chain.spec
   *  Get all chain which match {@link SearchParamsChain}
   */
  async getChains(searchParamsChain: SearchParamsChain): Promise<Response<ChainInfo[]>> {
    const chainService = await this.getChainService()
    return chainService.getChains(searchParamsChain);
  }

  /**
    tested @ccf.chain.spec

   This function creates a new block in the given chain with meta data information which can be seen below.
   This function does not modify existing data, it only appends new data.

    @param chainInfoBase The values of {@link ChainInfoBase} are used to update the chains meta data
    @return {@link Response.isOk} is true if the meta data was updated successfully, otherwise false

  */
  async updateChain(chainInfoBase: ChainInfoBase): Promise<Response<void>> {
    const chainService = await this.getChainService()
    return chainService.updateChain(chainInfoBase);
  }

  /**
   * tested @ccf.chain.spec
   *
   *  Returns all chains which are owned by the keys connected with the current {@link CCF} instance
   */
  async getMyChains(): Promise<Response<ChainInfo[]>> {
    const searchParamsChain: SearchParamsChain = {
      ownerPublicKey: await this.serviceConfiguration.signingModuleClient.getPublicKeyBase64(),
    }
    const chainService = await this.getChainService()
    return chainService.getChains(searchParamsChain);
  }

  // ------------------------------------------------------------------------------------------------------------------------
  // BLOCKS
  // ------------------------------------------------------------------------------------------------------------------------

  /** @hidden */
  private async getBlockService(): Promise<BlockService> {
    return new BlockService(await this.getConfigurationWithSession());
  }

  /**
   tested @ccf.block.spec

   * @param chainId The chain for which the blocks should be received

  */
  async getAllBlocks(chainId: string): Promise<Response<Block[]>> {
    const blockService = await this.getBlockService();
    return  blockService.getBlocks(chainId, {});
  }

    /**
       tested @ccf.block.spec

       Returns the newest {@link Block} of the given chain

    */
  async getNewestBlock(chainId: string): Promise<Response<Block[]>> {
    const blockService = await this.getBlockService();
    return blockService.getNewestBlock(chainId);
  }

  /**
   * tested @ccf.block.spec
   *
   * Get blocks of a chain with match the given {@link SearchParamsBlock}
   *
   * @param chainId
   * @param searchParamsBlock
   *
  */
  async getBlocks(chainId: string, searchParamsBlock: SearchParamsBlock): Promise<Response<Block[]>> {
    const blockService = await this.getBlockService();
    return await blockService.getBlocks(chainId, searchParamsBlock);
  }

  /**
   * tested @ccf.block.spec
   *
   * Get a certain block in a certain chain
   *
   * @param chainId
   * @param blockId
  */
  async getBlockById(chainId: string, blockId: string): Promise<Response<Block[]>>{
    return (await this.getBlockService()).getBlockById(chainId, blockId);
  }

  /**
   * @param chainId  The id of the chain in which the block should be booked
   * @param cryptIdReceiver Receiver of the created block
   * @param type Application type of the block (Must be larger than 30). E.g. different kinds of contracts or documents can have different types to make deserialization or filtering easier.
   * @param payload Must be a simple JavaScript string. Use JSON.stringify(YOUR_OBJECT) to serialize your object into a Json-String. The CCF is converting this string into a base64 stirng before booking the block.
   * @param encryptPayload Indicates whether the payload should be encrypted or not. The payload encryption is done with AES-CBC using a 256-Bit key which is stored encrypted in {@link Block.symmetricKeySender} and {@link Block.symmetricKeyRecipient}
   * @returns Response.data contains the blockId, if the creation was successful
   */
  async createBlock(chainId: string, cryptIdReceiver: Signature, type: number = 30, payload: string, encryptPayload: boolean = false): Promise<Response<string>> {
    const blockService = await this.getBlockService();
    return blockService.createBlock(chainId, await this.serviceConfiguration.signingModuleClient.getCryptId(),cryptIdReceiver, type, payload, encryptPayload);
  }

  // ------------------------------------------------------------------------------------------------------------------------
  // User
  // ------------------------------------------------------------------------------------------------------------------------

  /** @hidden */
  private async getUserService(): Promise<UserService> {
    return new UserService(await this.getConfigurationWithSession());
  }

  /**
   *
   * tested @ccf.user.spec
   *
   * For each user there is a user chain, So every change on a user will be tracked by the C_Chain system
   *
   * @param username  Username of the user
   * @param email Email of the user
   * @param applicationData Data which allows to store application data for a user e.g age. Store custom data with JSON.stringify(YOUR_OBJECT)
   * @returns Response.isOk is true if {@link createUser} was successful, false otherwise
   */
  async createUser(username: string, email?: string, applicationData?: string): Promise<Response<void>>{
    const userService = await this.getUserService();
    const createResult =  await userService.createUser();
    if(!createResult.isOk){
      return new Response<void>(false, createResult.error);
    }
    return await userService.updateUser(username, email, applicationData);
  }

  /**
      tested @ccf.user.spec

   This function creates a new block in the user chain, who is belongs to the current {@link CCF}.
   This function does not modify existing data, it only appends new data.

   @returns {@link Response.isOk} true if update was successful, false otherwise

  */
  async updateUser(username: string, email?: string, applicationData?: string): Promise<Response<void>>{
    return (await this.getUserService()).updateUser(username, email, applicationData);
  }

  /**
      tested @ccf.user.spec

      @param userId Receive the user which has the given id

  */
  async getUserById(userId: string): Promise<Response<User[]>>{
    return (await this.getUserService()).getUserById(userId);
  }

  /**
    tested @ccf.user.spec

    @param publicKey Receive the user which has the given publicKey

  */
  async getUserByPublicKey(publicKey: string): Promise<Response<User[]>>{
    return (await this.getUserService()).getUserByPublicKey(publicKey);
  }

  /**
    tested @ccf.user.spec

    Returns the {@link User} object which belongs to the current {@link CCF} instance if one is present
  */
  async getMyUser(): Promise<Response<User[]>>{
    return (await this.getUserService()).getUserByPublicKey(this.serviceConfiguration.signingModuleClient.getCryptId().publicKey);
  }

  /**
    tested @ccf.user.spec

    Deletes the user which belongs to the publicKey which is hold in the current {@link CCF} instance
   * @returns Response.isOk is true if {@link deleteMyUser} was successful, false otherwise
  */
  async deleteMyUser(): Promise<Response<void>>{
    const myUser = await this.getMyUser();
    if(!myUser.isOk || myUser.data.length == 0){
      return new Response(true, new Error('No user to delete'));
    }
    return (await this.getUserService()).deleteUser(myUser.data[0].id);
  }

  /**
      tested @ccf.user.spec

      Receive users which match the given {@link SearchParamsUser}
  */
  async getUsers(searchParamsUser?: SearchParamsUser): Promise<Response<User[]>>{
    return (await this.getUserService()).getUsers(searchParamsUser);
  }

  // ------------------------------------------------------------------------------------------------------------------------
  // ACL
  // ------------------------------------------------------------------------------------------------------------------------

  /** @hidden */
  private async getAclService(): Promise<AclService> {
    return new AclService(await this.getConfigurationWithSession());
  }

  /**
   * tested @ccf.acl.spec
   * @param chainId  The chain for which the {@link AclEntryRequest} should be applied
   * @param aclEntryRequest Receiver of the created block
   * @returns Response.isOk is true if {@link createOrUpdateAclEntry} was successful, false otherwise
   */
  async createOrUpdateAclEntry(chainId: string, aclEntryRequest: AclEntryRequest): Promise<Response<void>> {
    aclEntryRequest.delete = false;
    return (await this.getAclService()).putAclKeepOther(chainId, [aclEntryRequest]);
  }

  /**
    tested @ccf.acl.spec

   * @param chainId  Id of the chain where the {@link AclEntryRequest} should be deleted
   * @param publicKey The publicKey determines which permissions will be deleted
   * @returns Response.isOk is true if {@link deleteAclEntry} was successful, false otherwise

*/
  async deleteAclEntry(chainId: string, publicKey: string): Promise<Response<void>> {
    return (await this.getAclService()).putAclDeleteOther(chainId, [new AclEntryRequest(publicKey, false, false, true)]);
  }

  /**
    tested @ccf.acl.spec

   * @param chainId Chain for which the ACL should be received

  */
  async getAcl(chainId: string): Promise<Response<ACL>> {
    return (await this.getAclService()).getAcl(chainId, {showAll: true});
  }
  /**
    tested @ccf.acl.spec

    * @param chainId Chain for which the ACL should be received
    * @param publicKey The publicKey works as additional search parameter on the given chain
  */
  async getAclByPublicKey(chainId: string, publicKey: string): Promise<Response<ACL>> {
    return (await this.getAclService()).getAcl(chainId, {publicKey: publicKey});
  }

  /**
   tested @ccf.block.spec

   * @param block Block which has a symmetrically encrypted payload
   * @returns Decrypted payload as base64 string
   */
  async getPayloadDecrypted(block: Block): Promise<string>{
   if(block == null || block.algorithm == KeyAlgorithm.PLAIN){
     return null;
   }
   if(await this.isSenderMe(block)){
     return await this.getDecryptedPayload(block.symmetricKeySender, block.initialisationVector, block.payload);
   }else if(this.isRecipientMe(block)){
     return await this.getDecryptedPayload(block.symmetricKeyRecipient, block.initialisationVector, block.payload);
   }else{
     return null;
   }
  }

  /** @hidden */
  private async getDecryptedPayload(encryptedSymmetricKey: string, iv: string, encryptedPayload: string): Promise<string>{
      const decryptedSymmetricKey = await this.serviceConfiguration.signingModuleClient.decrypt(encryptedSymmetricKey);
      const aes = new AESService();
      return await aes.decrypt(new EncryptionData(encryptedPayload, iv, decryptedSymmetricKey));
  }

  /** @hidden */
  private isSenderMe(block: Block): boolean{
    return block.cryptIDSender.publicKey == this.serviceConfiguration.signingModuleClient.getCryptId().publicKey;
  }

  /** @hidden */
  private isRecipientMe(block: Block): boolean{
    return block.cryptIDRecipient.publicKey == this.serviceConfiguration.signingModuleClient.getCryptId().publicKey;
  }

}
export default CCF;
export {
  Signature,
  SearchParamsUser,
  SearchParamsChain,
  SearchParamsBlock,
  AclEntryRequest,
  ACL,
  ACLEntry,
  CCFRsaKeyPair,
  CatenaKeyFormat,
  CatenaKeyType,
  CCFRsaKey,
  User,
  UserBase,
  BlockBase,
  Block,
  ChainInfoBase,
  ChainInfo,
  Response,
  KeyAlgorithm,
  Notes
};
