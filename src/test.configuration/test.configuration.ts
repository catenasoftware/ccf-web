import ServiceConfiguration from "../service/service.configuration";
import SessionService from "../service/session.service";
import RsaModule from "../crypto/rsa.module";
import AsymmetricModule from "../crypto/asymmetric.module";

export class TestConfiguration{

  public static readonly host = "api.cchain.prod.catena.software"
  public static readonly host_local = "localhost"
  public static readonly port = 443
  public static readonly rejectUnauthorized = true
  public static subscriptionID = '00000000-0000-0000-0000-0000000000000';

  getServiceConfiguration(asymmetricModule: AsymmetricModule): ServiceConfiguration{
    return new ServiceConfiguration(TestConfiguration.host, TestConfiguration.port, asymmetricModule,TestConfiguration.rejectUnauthorized, TestConfiguration.subscriptionID);
  }

  async getServiceConfigurationWithoutSession(): Promise<ServiceConfiguration>{
    const keyPair = await RsaModule.generateKeyPairDer();
    const module = await new RsaModule().importKeys(keyPair);
    return this.getServiceConfiguration(module);
  }

  async getServiceConfigurationWithSession(asymmetricModule: AsymmetricModule): Promise<ServiceConfiguration>{
    let serviceConfiguration = this.getServiceConfiguration(asymmetricModule);
    const sessionService = new SessionService(serviceConfiguration);
    const session = await sessionService.createNewSession();
    const cryptIdCCM = await sessionService.getCryptIdCCMFromCCM();
    serviceConfiguration.session = session.data;
    serviceConfiguration.cryptIdCCM = cryptIdCCM.data[0];
    return serviceConfiguration;
  }

}
