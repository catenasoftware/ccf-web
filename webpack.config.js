/* global __dirname, require, module*/

const webpack = require("webpack");
const path = require("path");
const yargs = require("yargs");
const env = yargs.argv.env; // use --env with webpack 2
const pkg = require("./package.json");
const shouldExportToAMD = yargs.argv.amd;

let libraryName = pkg.name;

let outputFile, mode;

if (shouldExportToAMD) {
  libraryName += ".amd";
}

if (env === "build") {
  mode = "production";
  outputFile = libraryName + ".min.js";
} else {
  mode = "development";
  outputFile = libraryName + ".js";
}

const config = {
  mode: mode,
  entry: __dirname + "/src/ccf.ts",
  devtool: "source-map",
  output: {
    path: __dirname + "/dist",
    filename: outputFile,
    library: libraryName,
    // libraryTarget: shouldExportToAMD ? "amd" : "umd",
    libraryTarget: "umd",
    libraryExport: "default",
    umdNamedDefine: true,
    globalObject: "typeof self !== 'undefined' ? self : this",
  },
  module: {
    rules: [
      {
        test: /(\.jsx|\.js|\.ts|\.tsx)$/,
        use: {
          loader: "ts-loader",
        },
        exclude: /(node_modules|bower_components)/,
      },
    ],
  },
  resolve: {
    modules: [path.resolve("./node_modules"), path.resolve("./src")],
    extensions: [".ts",".json", ".js"],
    alias:  {
      "crypto": require.resolve("crypto-browserify"),
      "buffer": require.resolve("buffer/"),
      "stream": require.resolve("stream-browserify"),
      "https": require.resolve("https-browserify"),
      "url": require.resolve("url/"),
      "http": require.resolve("stream-http"),
      "constants": require.resolve("constants-browserify"),
      "assert": require.resolve("assert/")
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
    process: 'process/browser',
  }),
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    })]
};
// yarn config set  ‘https://gitlab.db.in.tum.de/c-chain/ccf-kotlin-repo/0.0.1/packages/npm/:_authToken' "AP_WRyg98APHTbxV9dVw"
module.exports = config;
