# C_Chain Foundation (CCF) Web

This JavaScript SDK is developed by Catena GmbH and provides all functions to communicate with the C_Chain Manager (CCM). 

## Functionalities

- Handle User
- Handle Chains
- Hanlde Blocks
- Handle ACLs

The CCF class is the entry point for all calls.

## Cryptography

This SDK is using the [Web Crypto API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API), which is available in the browser (See [Browser compatibility](https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API#browser_compatibility)). In a non-browser environment the [Node Implementation](https://nodejs.org/api/webcrypto.html) of the API will be used.   



## Documentation

- Execute 'yarn doc' to generate this documentation.




